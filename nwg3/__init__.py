# coding: utf-8
import numpy as np
import random
from ast import literal_eval

#random.seed(30)

def condicionesiniciales(numnodos, numnodosvivos, matriz1, matriz2, matriz3, matriz1a, matriz1b):
    """
    recibe el numero de nodos total (22) y el numero de nodos a trabajar en cada experimento (2, 3, 4, etc)
    """
    e=np.random.uniform(low=0, high=1.0, size=(numnodos,)) #vector e para cada nodo un número al azar entre 0 y 1
    m=np.random.uniform(low=1, high=10, size=(numnodos,)) #vector m de para cada nodo un número al azar entre 1 y 10
    #hacer la mortalidad de la especie basal negativa para simular que crece en ausencia de las demas especies y proporcional al num de especies en la red:
    m[4]=m[4]*-1*(numnodosvivos-1)
    n00=np.random.randint(10,100,numnodos) #condiciones iniciales para cada nodo entre 10 y 100
    a=matriz1 
    a1=matriz1a
    a2=matriz1b
    a=a.values #lo convierte a numpy.array
    a1=a1.values
    a2=a2.values
    h=np.random.uniform(low=.01, high=.1, size=(numnodos,numnodos))#matriz h  que multiplica a la matriz 1a entre 0.01 y 0.1 
    h=h*a1
    #np.fill_diagonal(a, -1, wrap=False) #pongo diagonal negativa para simular competencia intraespecifica
    aleatoriosiguales=np.random.rand(numnodos,numnodos)
    a=a*aleatoriosiguales
    a1=a1*aleatoriosiguales
    a2=a2*aleatoriosiguales
    
    d=matriz2*0
    d=d.values
    p=matriz3*0
    p=p.values

    return e, h, m, a, d, p, n00, a1, a2




def modelocrecered (n, t, e, h, m, a1, a2, d, p, matriz1, matriz2, a, n0):

    interacciones1=[]
    for i in range(0,22):
        for j in range(0,22):
            if matriz1.iloc[i,j]!=0:
                interacciones1.append((i,j))

            
    interacciones2=[]
    interacciones2b=[]
    for i in range(0,22):
        for j in range(0,66):
            if matriz2.iloc[i,j]!=0:
                interacciones2.append((i,matriz2.columns[j]))
                
    interacciones2=np.array(interacciones2)    #todo esto es para que los elementos en la lista se reocnozcan como int y no como string
    for i in range(0,len(interacciones2)):
        interacciones2b.append((literal_eval(interacciones2[i,0]),literal_eval(interacciones2[i,1])))

    
    dndt=[]
    for i in range (0,len(a)): #para cada especie 
        
        if n0[i]==0.0: #para que se salte las especies que están en población cero (nodos aún no añadidos)
            dndti=0
            dndt.append(dndti)            
        else:
            
            rfuncional= 0 #bien colocado
            rfuncional2= 0 #bien colocado
            for k in range (0, len(a)): #para cada presa

                inhibicion=0 #bien colocado
                if (i,k) not in interacciones1: #no interactuan
                    pass

                else:                #sí hay interaccion depredador presa
                    arista = interacciones1.index((i,k)) #te da el index de la interacción de 1 orden entre i,k

                    for q in range (0, len(d)): #para cada inhibidor
                        inhibiciondelainhibicion=0 #bien colocado

                        if (q, (i, k)) not in interacciones2b:  #no inhibe 
                            pass

                        else:             #sí hay inhibición de la depredación
                            #aristazul=interacciones2b.index((q,(i,k)))

                            for r in range (0, len(p)): #para cada inhibidor del inhibidor
                                multiplicacion=0
                                #multiplicacion=p[r,aristazul]*n[r]
                                inhibiciondelainhibicion+=multiplicacion

                        sumainhib=d[q,arista]*n[q]/(1+inhibiciondelainhibicion)
                        inhibicion+=sumainhib

                numerador = a1[i,k]*n[k]
                denominador= 1+a1[i,k]*h[i,k]*n[k]+inhibicion
                division=numerador/denominador
                rfuncional+= division
                numerador2=a2[i,k]*n[k]
                denominador2= 1+a2[i,k]*h[k,i]*n[i]+inhibicion
                division2=numerador2/denominador2
                rfuncional2+=division2

            valor_1= e[i]*rfuncional*n[i]
            valor_2= rfuncional2*n[i]
            dndti= valor_1 - m[i]*n[i] + valor_2
            dndt.append(dndti)
    return dndt



def creared8 (matriz1):
    #escojo al azar los nodos que conformarán la red

    n0falso=np.zeros(22)
    n0falso[4]=1 #café agregado
    #escojo cualquier nodo conectado directamente al café
    #Si en el renglon o columna del cafe hay algo diferente de cero, agregar el index de la columna o el renglon a una lista
    nodosposibles=[]
    for i in range (0,len(matriz1)):
        if matriz1.iloc[4,i]!=0:
            nodosposibles.append(i)
        if matriz1.iloc[i,4]!=0:
            nodosposibles.append(i)
    nodosposibles=list(set(nodosposibles)) #quito los repetidos
    #escoger al azar un index de esa lista, ese será el nodoagregado
    nodoagregado=random.choice(nodosposibles)
    n0falso[nodoagregado]=1
    #hacer lista con nodos que conectan directo con los nodos 1 y 2 del mismo modo que arriba
    nodosposibles2=[]
    for i in range (0,len(matriz1)):
        if matriz1.iloc[4,i]!=0:
            nodosposibles2.append(i)
        if matriz1.iloc[i,4]!=0:
            nodosposibles2.append(i)
        if matriz1.iloc[nodoagregado,i]!=0:
            nodosposibles2.append(i)
        if matriz1.iloc[i, nodoagregado]!=0:
            nodosposibles2.append(i)
    nodosposibles2=list(set(nodosposibles2)) #quito los repetidos
    #quitar de la lista los nodos que ya fueron agregados
    nodosposibles2.remove(4)
    nodosposibles2.remove(nodoagregado)
    #elegir al azar un elemento de la lista
    nodoagregado2=random.choice(nodosposibles2)
    n0falso[nodoagregado2]=1
    #hacer lista con nodos que conecten directo con nodos 1, 2 y 3
    nodosposibles3=[]
    for i in range (0,len(matriz1)):
        if matriz1.iloc[4,i]!=0:
            nodosposibles3.append(i)
        if matriz1.iloc[i,4]!=0:
            nodosposibles3.append(i)
        if matriz1.iloc[nodoagregado,i]!=0:
            nodosposibles3.append(i)
        if matriz1.iloc[i, nodoagregado]!=0:
            nodosposibles3.append(i)
        if matriz1.iloc[nodoagregado2,i]!=0:
            nodosposibles3.append(i)
        if matriz1.iloc[i, nodoagregado2]!=0:
            nodosposibles3.append(i)
    nodosposibles3=list(set(nodosposibles3)) #quito los repetidos
    #quitar de la lista los nodos que ya fueron agregados
    nodosposibles3.remove(4)
    nodosposibles3.remove(nodoagregado)
    nodosposibles3.remove(nodoagregado2)
    #elegir al azar un elemento de la lista
    nodoagregado3=random.choice(nodosposibles3)
    n0falso[nodoagregado3]=1
    #hacer lista con nodos que conecten directo con nodos 1, 2, 3 y 4
    nodosposibles4=[]
    for i in range (0,len(matriz1)):
        if matriz1.iloc[4,i]!=0:
            nodosposibles4.append(i)
        if matriz1.iloc[i,4]!=0:
            nodosposibles4.append(i)
        if matriz1.iloc[nodoagregado,i]!=0:
            nodosposibles4.append(i)
        if matriz1.iloc[i,nodoagregado]!=0:
            nodosposibles4.append(i)
        if matriz1.iloc[nodoagregado2,i]!=0:
            nodosposibles4.append(i)
        if matriz1.iloc[i,nodoagregado2]!=0:
            nodosposibles4.append(i)
        if matriz1.iloc[nodoagregado3,i]!=0:
            nodosposibles4.append(i)
        if matriz1.iloc[i,nodoagregado3]!=0:
            nodosposibles4.append(i)
    nodosposibles4=list(set(nodosposibles4)) #quito los repetidos
    #quitar de la lista los nodos que ya fueron agregados
    nodosposibles4.remove(4)
    nodosposibles4.remove(nodoagregado)
    nodosposibles4.remove(nodoagregado2)
    nodosposibles4.remove(nodoagregado3)
    #elegir al azar un elemento de la lista
    nodoagregado4=random.choice(nodosposibles4)
    n0falso[nodoagregado4]=1
    #hacer lista con nodos que conectan a 1,2,3,4 y 5
    nodosposibles5=[]
    for i in range (0,len(matriz1)):
        if matriz1.iloc[4,i]!=0:
            nodosposibles5.append(i)
        if matriz1.iloc[i,4]!=0:
            nodosposibles5.append(i)
        if matriz1.iloc[nodoagregado,i]!=0:
            nodosposibles5.append(i)
        if matriz1.iloc[i,nodoagregado]!=0:
            nodosposibles5.append(i)
        if matriz1.iloc[nodoagregado2,i]!=0:
            nodosposibles5.append(i)
        if matriz1.iloc[i,nodoagregado2]!=0:
            nodosposibles5.append(i)
        if matriz1.iloc[nodoagregado3,i]!=0:
            nodosposibles5.append(i)
        if matriz1.iloc[i,nodoagregado3]!=0:
            nodosposibles5.append(i)
        if matriz1.iloc[nodoagregado4,i]!=0:
            nodosposibles5.append(i)
        if matriz1.iloc[i,nodoagregado4]!=0:
            nodosposibles5.append(i)
    nodosposibles5=list(set(nodosposibles5)) #quito los repetidos
    #quitar de la lista los nodos que ya fueron agregados
    nodosposibles5.remove(4)
    nodosposibles5.remove(nodoagregado)
    nodosposibles5.remove(nodoagregado2)
    nodosposibles5.remove(nodoagregado3)
    nodosposibles5.remove(nodoagregado4)
    #elegir al azar un elemento de la lista
    nodoagregado5=random.choice(nodosposibles5)
    n0falso[nodoagregado5]=1
    #hacer lista para nodo 7
    nodosposibles6=[]
    for i in range (0,len(matriz1)):
        if matriz1.iloc[4,i]!=0:
            nodosposibles6.append(i)
        if matriz1.iloc[i,4]!=0:
            nodosposibles6.append(i)
        if matriz1.iloc[nodoagregado,i]!=0:
            nodosposibles6.append(i)
        if matriz1.iloc[i,nodoagregado]!=0:
            nodosposibles6.append(i)
        if matriz1.iloc[nodoagregado2,i]!=0:
            nodosposibles6.append(i)
        if matriz1.iloc[i,nodoagregado2]!=0:
            nodosposibles6.append(i)
        if matriz1.iloc[nodoagregado3,i]!=0:
            nodosposibles6.append(i)
        if matriz1.iloc[i,nodoagregado3]!=0:
            nodosposibles6.append(i)
        if matriz1.iloc[nodoagregado4,i]!=0:
            nodosposibles6.append(i)
        if matriz1.iloc[i,nodoagregado4]!=0:
            nodosposibles6.append(i)
        if matriz1.iloc[nodoagregado5,i]!=0:
            nodosposibles6.append(i)
        if matriz1.iloc[i,nodoagregado5]!=0:
            nodosposibles6.append(i)
    nodosposibles6=list(set(nodosposibles6)) #quito los repetidos
    #quitar de la lista los nodos que ya fueron agregados
    nodosposibles6.remove(4)
    nodosposibles6.remove(nodoagregado)
    nodosposibles6.remove(nodoagregado2)
    nodosposibles6.remove(nodoagregado3)
    nodosposibles6.remove(nodoagregado4)
    nodosposibles6.remove(nodoagregado5)
    #elegir al azar un elemento de la lista
    nodoagregado6=random.choice(nodosposibles6)
    n0falso[nodoagregado6]=1
    #hacer lista para nodo 8
    nodosposibles7=[]
    for i in range (0,len(matriz1)):
        if matriz1.iloc[4,i]!=0:
            nodosposibles7.append(i)
        if matriz1.iloc[i,4]!=0:
            nodosposibles7.append(i)
        if matriz1.iloc[nodoagregado,i]!=0:
            nodosposibles7.append(i)
        if matriz1.iloc[i,nodoagregado]!=0:
            nodosposibles7.append(i)
        if matriz1.iloc[nodoagregado2,i]!=0:
            nodosposibles7.append(i)
        if matriz1.iloc[i,nodoagregado2]!=0:
            nodosposibles7.append(i)
        if matriz1.iloc[nodoagregado3,i]!=0:
            nodosposibles7.append(i)
        if matriz1.iloc[i,nodoagregado3]!=0:
            nodosposibles7.append(i)
        if matriz1.iloc[nodoagregado4,i]!=0:
            nodosposibles7.append(i)
        if matriz1.iloc[i,nodoagregado4]!=0:
            nodosposibles7.append(i)
        if matriz1.iloc[nodoagregado5,i]!=0:
            nodosposibles7.append(i)
        if matriz1.iloc[i,nodoagregado5]!=0:
            nodosposibles7.append(i)
        if matriz1.iloc[nodoagregado6,i]!=0:
            nodosposibles7.append(i)
        if matriz1.iloc[i, nodoagregado6]!=0:
            nodosposibles7.append(i)
    nodosposibles7=list(set(nodosposibles7)) #quito los repetidos
    #quitar de la lista los nodos que ya fueron agregados
    nodosposibles7.remove(4)
    nodosposibles7.remove(nodoagregado)
    nodosposibles7.remove(nodoagregado2)
    nodosposibles7.remove(nodoagregado3)
    nodosposibles7.remove(nodoagregado4)
    nodosposibles7.remove(nodoagregado5)
    nodosposibles7.remove(nodoagregado6)
    #elegir al azar un elemento de la lista
    nodoagregado7=random.choice(nodosposibles7)
    n0falso[nodoagregado7]=1

    return n0falso
