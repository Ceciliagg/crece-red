#!/usr/bin/env python
# coding: utf-8

import argparse
import pandas as pd
import numpy as np
from scipy.integrate import LSODA
from scipy.integrate import odeint
import os
import warnings
from nwg import matricesindirectas, modelowith
from os import path
from sh import mkdir
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


parser = argparse.ArgumentParser(description='lee todos los archivos guardados para agregar interacciones de alto orden')
parser.add_argument('--code', help='previous run code', required=True)
parser.add_argument('--subdir', help='subdir with followup files', required=True)
args = parser.parse_args()

clave = args.code
print(clave)
numnodos = 4 ###########################

matriz1 = pd.read_csv('matriz1.csv', index_col=0)
matriz2 = pd.read_csv('matriz2.csv', index_col=0)
matriz3 = pd.read_csv('matriz3.csv', index_col=0)
t=np.linspace(0,100,1000)

a=np.loadtxt(path.join(args.subdir, 'a_si_'+clave))
e=np.loadtxt(path.join(args.subdir, 'e_si_'+clave))
h=np.loadtxt(path.join(args.subdir, 'h_si_'+clave))
m=np.loadtxt(path.join(args.subdir, 'm_si_'+clave))
n0=np.loadtxt(path.join(args.subdir, 'n0_si_'+clave))

a1=a.copy()
a2=a.copy()
for i in range(numnodos):
    for k in range(numnodos):
        if a1[i,k]<0:
            a1[i,k]=0
        else:
            pass
        
for i in range(numnodos):
    for k in range(numnodos):
        if a2[i,k]>0:
            a2[i,k]=0
        else:
            pass


while True:
    (d, p) = matricesindirectas(matriz2, matriz3)
    
    print('genero d,p e integro')
    # integro
    np.seterr(all='warn')
    warnings.filterwarnings("error")

    try:
        N = odeint(modelowith, n0, t, args=(e, h, m, a1, a2, d, p, matriz1, matriz2, a, n0))
        break
    except:
        continue
print('listo')
vivos = [i for i in range(len(n0)) if n0[i]>0] #index de las especies presentes en la red

outdir = 'with4'         ########################################################
mkdir('-p', outdir)
    
np.savetxt(path.join(outdir,
                     'd_%s_%s' % ('si', clave)),
           d)
np.savetxt(path.join(outdir,
                     'p_%s_%s' % ('si', clave)),
           p)
np.savetxt(path.join(outdir,
                     'Nwith_%s_%s' % ('si', clave)),
           N)
for i in vivos:
    plt.plot(t,N[:,i], linewidth=2, label=matriz1.index[i])
plt.xlabel('tiempo')
plt.ylabel('N(t)')
plt.legend()
plt.savefig(path.join(outdir,
                      'plotwith_%s_%s.png' % ('si', clave)))
plt.clf()
