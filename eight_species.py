#!/usr/bin/env python
# coding: utf-8

#cinco especies sin indirectas
import pandas as pd
import numpy as np
from scipy.integrate import LSODA
from scipy.integrate import odeint
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import datetime
import os
import warnings
from nwg3 import condicionesiniciales, modelocrecered, creared8
from os import path
import uuid
from sh import mkdir


matriz1 = pd.read_csv('matriz1.csv', index_col=0)
matriz2 = pd.read_csv('matriz2.csv', index_col=0)
matriz3 = pd.read_csv('matriz3.csv', index_col=0)
matriz1a = pd.read_csv('matriz1a.csv', index_col=0)
matriz1b = pd.read_csv('matriz1b.csv', index_col=0)


t=np.linspace(0,100,1000)

altoorden=[(4, 8, 16),
           (0, 4, 16),
           (16, 19, 21),
           (16, 18, 21),
           (7, 16, 19),
           (11, 19, 21),
           (7, 11, 19),
           (4, 8, 13, 16),
           (0, 4, 13, 16),
           (13, 16, 19, 21),
           (13, 16, 18, 21),
           (7, 13, 16, 19)]



n0falso=creared8(matriz1)
while True:
    (e, h, m, a, d, p, n0, a1, a2) = condicionesiniciales(22, 8, matriz1, matriz2, matriz3, matriz1a, matriz1b)
    
    n0 = n0 * n0falso
    
    # integro
    np.seterr(all='warn')
    warnings.filterwarnings("error")

    try:
        N = odeint(modelocrecered, n0, t, args=(e, h, m, a1, a2, d, p, matriz1, matriz2, a, n0))
        break
    except:
        continue
    
    
# cuento si hubo posibilidad de interacciones indirectas
vivos = [i for i in range(len(n0)) if n0[i] > 0] #me da el index de las especies presentes en la red
lala = 0 #para contar
for i in range(len(altoorden)): #reviso si entre las presentes estan quellas necesarias para que haya interacciones de alto orden
    check=all(item in vivos for item in altoorden[i])
    if check is True:
        lala+=1
    else:
        pass
if lala > 0:  #si al menos uno de los conjuntos de nodos con interacciones de alto orden está presente
    marca='si' #para poner en nombre un marcador si tiene indirectas potencialmente
else:
    marca='no'

save_id = uuid.uuid4()
    
if all(N[-1,i] > 1 for i in vivos): #si todos los nodos originalmente presentes terminan con población mayor a 1
    outdir = 'todasviven8'
    mkdir('-p', outdir)
    
    np.savetxt(path.join(outdir,
                         'vivos_%s_%s.txt' % (marca, save_id)),
               vivos)
    np.savetxt(path.join(outdir,
                         'e_%s_%s.txt' % (marca, save_id)),
               e)
    np.savetxt(path.join(outdir,
                         'h_%s_%s.txt' % (marca, save_id)),
               h)
    np.savetxt(path.join(outdir,
                         'm_%s_%s.txt' % (marca, save_id)),
               m)
    np.savetxt(path.join(outdir,
                         'a_%s_%s.txt' % (marca, save_id)),
               a)
    np.savetxt(path.join(outdir,
                         'n0_%s_%s.txt' % (marca, save_id)),
               n0)
    np.savetxt(path.join(outdir,
                         'N_%s_%s.txt' % (marca, save_id)),
               N)
    for i in vivos:
        plt.plot(t,N[:,i], linewidth=2, label=matriz1.index[i])
    plt.xlabel('tiempo')
    plt.ylabel('N(t)')
    plt.legend()
    plt.savefig(path.join(outdir,
                          'plot_%s_%s.png' % (marca, save_id)))
    plt.clf()
else:
    outdir = 'notodasviven8'
    mkdir('-p', outdir)
    
    np.savetxt(path.join(outdir,
                         'vivos_%s_%s.txt' % (marca, save_id)),
               vivos)
    np.savetxt(path.join(outdir,
                         'e_%s_%s.txt' % (marca, save_id)),
               e)
    np.savetxt(path.join(outdir,
                         'h_%s_%s.txt' % (marca, save_id)),
               h)
    np.savetxt(path.join(outdir,
                         'm_%s_%s.txt' % (marca, save_id)),
               m)
    np.savetxt(path.join(outdir,
                         'a_%s_%s.txt' % (marca, save_id)),
               a)
    np.savetxt(path.join(outdir,
                         'n0_%s_%s.txt' % (marca, save_id)),
               n0)
    np.savetxt(path.join(outdir,
                         'N_%s_%s.txt' % (marca, save_id)),
               N)
    for i in vivos:
        plt.plot(t,N[:,i], linewidth=2, label=matriz1.index[i])
    plt.xlabel('tiempo')
    plt.ylabel('N(t)')
    plt.legend()
    plt.savefig(path.join(outdir,
                          'plot_%s_%s' % (marca, save_id)))
    plt.clf()
